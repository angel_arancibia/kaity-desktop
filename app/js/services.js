'use strict';

/* Services */

kaityApp.factory("SessionService", function (Restangular, $location, LoggedInRestangular, storage, PermissionService,$q, $window) {
  var loggedIn = false;
  var authToken = null;
  var permissions = null;
  var email = null;
  var adminArea = null;

  return {
    isLogged: function(){
        var that = this;
        console.log("isLogged? ", that.loggedIn);
        return that.loggedIn;
    },
    getToken: function(){
      return authToken;
    },
    getPermissions: function(){
      var that = this;
      return that.permissions;
    },
    getEmail: function(){
      var that = this;
      console.log("getEmail:", that.email);
      return that.email;
    },
    canAdminArea: function(){
      var that = this;
      console.log("canAdminArea? ", that.adminArea);

      if (that.adminArea === true){
        return true;
      }
      return false;
    },

    recover: function(){
      var that = this;
      console.log("recover!");
      if (!!storage.get("loggedIn")) {
        console.log("tenemos cookie", storage.get("loggedIn"));
        that.buildSessionData(storage.get("authToken")).then( function(){
          if (that.canAdminArea) {
            $location.path("admin");
          }
        });
      }
      else {
        console.log("no tenemos cookie");
      }
    },

    login: function(credentials, $scope){
      var that = this;
      console.log("login ... ", credentials);
      Restangular.all('users/sign_in').post({user_login: {email: credentials.email, password:credentials.password}}).then(function(response) {
        console.log("login ok");
        that.buildSessionData(response.auth_token).then( function(){
          $('#loginModal').foundation('reveal', 'close');
          if (that.canAdminArea) {
            $location.path("admin");
          }
        });
      }, function(response) {
        // TODO: emitir mensaje de error
        $scope.message = "Usuario o contraseña incorrecta.";
        $('#loginMessage').show();
        console.log("Error with status code", response.status);
      });
    },

    logout: function(){
      var that = this;
      that.loggedIn = false;
      that.email = null;

      LoggedInRestangular.get().all('users').customDELETE('sign_out').then(function(response) {
        that.destroy();
        console.log("logout ok", response);
      }, function(response) {
        that.destroy();
        console.log("Error with status code", response.status);
      });
    },

    destroy: function(){
      var that = this;
      that.loggedIn = false;
      that.email = null;
      that.authToken = "";
      storage.clearAll();
      //$location.path("/");
      $window.location.ref = '/';
    },

    buildSessionData: function(token){
      //var deferred = $q.defer();
      var that = this;
      var deferred = $q.defer();

      setTimeout(function() {
        that.loggedIn = true;
        that.authToken = token;

        storage.set("loggedIn",true);
        storage.set("authToken",token);

        LoggedInRestangular.init(token);
        LoggedInRestangular.get().all('sessions').customGET('full_session').then(function(response) {
          console.log("raw permissions: ", response.permissions);
          PermissionService.init(response.permissions);
          that.permissions = PermissionService;
          that.email = response.email;
          that.adminArea = response.admin_area;
          console.log("email: ", response.email);
          console.log("adminArea: ", response.admin_area);
          console.log("permissions: ", that.permissions.get());
          deferred.resolve();
        }, function(response){
          console.log("Error with status code", response.status);
          deferred.reject("bad call");
        });
      },1000);

      return deferred.promise;
    }

  };
});

kaityApp.factory('PermissionService', function() {
  var service = {};
  var permissions = null;

  service.init = function(raw_permissions) {
    permissions = raw_permissions;
  };

  service.get = function() {
    return permissions;
  };

  service.can = function(resource, action) {
    if (permissions[resource][action] === true){
      return true;
    }
    return false;
  };

  return service;
});

kaityApp.factory('LoggedInRestangular', function(Restangular) {
  var service = {};
  var instance = null;
  service.init = function(token) {
    instance = Restangular.withConfig(function(RestangularConfigurer) {
      /* falta agrgarle CSRF token */
      RestangularConfigurer.setDefaultRequestParams({ auth_token: token });
    });
  };

  service.get = function() {
    return instance;
  };

  return service;
});


kaityApp.factory("FlashService", function($rootScope) {
  return {
    show: function(message) {
      $rootScope.flash = message;
    },
    clear: function() {
      $rootScope.flash = "";
    }
  };
});
