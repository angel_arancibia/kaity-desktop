'use strict';

function LoginCtrl($scope, SessionService){

  $scope.credentials = {};
  $scope.message = "";

  $scope.login = function() {
    $scope.reset_message();
    SessionService.login($scope.credentials, $scope);
  };

  $scope.signup = function(){
    $scope.reset_message();
    $scope.messageSignUp = "Falta conectar con API. Sory";
    $('#signUpMessage').show();
  };

  $scope.reset_message = function(){
    console.log("reset");
    $('#loginMessage').hide();
    $('#signUpMessage').hide();
  };
}

function SessionCtrl($scope,SessionService, $location){
  $scope.email = SessionService.getEmail();
  $scope.is_logged_in = SessionService.isLogged();

  $scope.logout = function() {
    console.log("logout started");
    SessionService.logout();
    console.log("logout ended");
  };
  
  $scope.recover = function(){
    SessionService.recover();
  };

  $scope.check_logged_in = function(){
    if (! SessionService.isLogged()){
      $location.path('/');
    }
    console.log("check_logged_in");
  };

  $scope.check_can_admin = function(){
    if (! SessionService.canAdminArea()){
      $location.path('/');
    }
    console.log("check_can_admin");
  };

}

function MenuCtrl($scope,$location){
  $scope.isCurrentPath = function (path){
    return $location.path() == path;
  };
}

function MainCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}

function UserNewCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}

function UserListCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}

function UserEditCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}

function BusesCtrl($scope, SessionService, LoggedInRestangular) {
  $scope.buses = [];
  $scope.bus = {};

  $scope.index = function() {
    LoggedInRestangular.get().all('buses').getList().then(function(response) {
      $scope.buses = response;
      console.log("buses:", $scope.buses);
    }, function() {
      $scope.buses = [];
      console.log("There was an error");
    });
  };

  $scope.show = function(bus){
    $scope.bus = bus;
  };

  $scope.edit = function(){
    $scope.bus.put().then(function(response) {
      console.log("Edit okay");
    }, function() {
      console.log("There was an error");
    });
  };

  $scope.create = function() {
    LoggedInRestangular.get().all('buses').post($scope.bus).then(function() {
      console.log("Object saved OK");
    }, function() {
      console.log("There was an error saving");
    });
  };

  $scope.remove = function(confirm){
    if (confirm) {
      $scope.bus.remove().then(function() {
        console.log("Object remove OK");
      }, function() {
        console.log("There was an error removing");
      });
    }
    $scope.bus = {};
  };

}

function AuditorsCtrl($scope, SessionService, LoggedInRestangular) {
  $scope.history_audits = [];
  $scope.audits = [];
  $scope.history_object = {};
  $scope.search_params = {};

  $scope.imAnUpdate = function(values){
    if (typeof values === "string" ) { return false; }
    else {return true; }
  };

  $scope.index = function() {
    $scope.common_retrieve('auditors');
  };

  $scope.buses = function(){
    $scope.common_retrieve('auditors/buses');
  };

  $scope.users = function(){
    $scope.common_retrieve('auditors/users');
  };

  $scope.sessions = function(){
    $scope.common_retrieve('auditors/sessions');
  };

  $scope.get_history = function(object_class, object_id){
    $scope.history_object.class = object_class;
    $scope.history_object.id = object_id;
    $scope.history_audits = [];

    LoggedInRestangular.get().all('auditors/history').post({class: object_class, id: object_id }).then(function(response) {
      $scope.history_audits = response;
      console.log("ok", response);
    }, function() {
      $scope.history_audits = [];
      console.log("There was an error");
    });
  };

  $scope.search = function(){
    $scope.audits = [];
    console.log("parametros:" , $scope.search_params,$scope.search_params.to, $scope.search_params.from);
    LoggedInRestangular.get().all('auditors/search').post($scope.search_params).then(function(response) {
      $scope.audits = response;
      console.log("ok", response);
    }, function() {
      $scope.history_audits = [];
      console.log("There was an error");
    });
  };

  $scope.common_retrieve = function(path){
    $scope.audits = [];
    LoggedInRestangular.get().all(path).getList().then(function(response) {
      $scope.audits = response;
      $('.from').datetimepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateText, inst) {
          $scope.$apply(function () {
            $scope.search_params.from = dateText;
          });
        }
      });
      $('.to').datetimepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateText, inst) {
          $scope.$apply(function () {
            $scope.search_params.to = dateText;
          });
        }
      });
    }, function() {
      $scope.audits = [];
      console.log("There was an error");
    });
  };

}

function StopsListCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}
function StopsNewCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}
function StopsEditCtrl($scope) {
  //$scope.phoneId = $routeParams.phoneId;
}

function StopsCtrl($scope, SessionService, LoggedInRestangular) {
  $scope.stops = [];
  $scope.stop = {};

  $scope.index = function() {
    LoggedInRestangular.get().all('stops').getList().then(function(response) {
      $scope.stops = response;
      console.log("stops:", $scope.stops);
    }, function() {
      $scope.stops = [];
      console.log("There was an error");
    });
  };

  $scope.show = function(stop){
    $scope.stop = stop;
  };

  $scope.edit = function(){
    $scope.stop.put().then(function(response) {
      console.log("Edit okay");
    }, function() {
      console.log("There was an error");
    });
  };

  $scope.create = function() {
    LoggedInRestangular.get().all('stops').post($scope.stop).then(function() {
      console.log("Object saved OK");
    }, function() {
      console.log("There was an error saving");
    });
  };

  $scope.remove = function(confirm){
    if (confirm) {
      $scope.stop.remove().then(function() {
        console.log("Object remove OK");
      }, function() {
        console.log("There was an error removing");
      });
    }
    $scope.stop = {};
  };
}

function ResultListCtrl($scope) {
    $scope.results = [
      {
      "line": "108",
      "stop": "San Martin y Mendoza",
      "time" : "12:35"
      },
      {
      "line": "108",
      "stop": "San Martin y Mendoza",
      "time" : "12:40"
      },
      {
      "line": "108",
      "stop": "San Martin y Mendoza",
      "time" : "12:45"
      }
    ];
}

function FavoriteListCtrl($scope) {
    $scope.favorites = [
      {
      "id" : "1",
      "line": "108",
      "stop": "San Martin y Mendoza"
      },
      {
      "id" : "2",
      "line": "108",
      "stop": "Alem y Mendoza"
      },
      {
      "id" : "3",
      "line": "108",
      "stop": "Oroño y Cordoba"
      },
      {
      "id" : "4",
      "line": "35/9",
      "stop": "Oroño y Cordoba"
      }
    ];
}