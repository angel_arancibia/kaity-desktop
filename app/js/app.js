'use strict';

var kaityApp = angular.module('kaity', ['ui.router','restangular', 'angularLocalStorage']);

kaityApp.config(function($stateProvider, $urlRouterProvider){
  //
  // For any unmatched url, send to /main
  $urlRouterProvider.otherwise("/main");
  //
  // Now set up the states
  $stateProvider
    .state('main', {
        url: "/main",
        templateUrl: "partials/main/index.html"
    })
      .state('main.login', {
          url: "/login",
          templateUrl: "partials/main/login.html",
          controller: 'LoginCtrl'
      })          
    .state('admin', {
        url: "/admin",
        templateUrl: "admin.html"
    })
      .state('admin.users', {
          url: "/users",
          templateUrl: "partials/users/index.html",
          controller: 'UserListCtrl'
      })
      .state('admin.buses', {
          url: "/buses",
          templateUrl: "partials/buses/index.html",
          controller: 'BusesCtrl'
      })
      .state('admin.stops', {
          url: "/stops",
          templateUrl: "partials/stops/index.html",
          controller: 'StopsListCtrl'
      })
      .state('admin.auditors', {
          url: "/auditors",
          templateUrl: "partials/auditors/index.html",
          controller: 'AuditorsCtrl'
      })
        .state('admin.auditors.general',{
          url: "/general",
          templateUrl: "partials/auditors/general.html",
          controller: 'AuditorsCtrl'
        })
        .state('admin.auditors.bus',{
          url: "/bus",
          templateUrl: "partials/auditors/bus.html",
          controller: 'AuditorsCtrl'
        });
});

kaityApp.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl("http://api.kaity.local:3000/api/v1");
    RestangularProvider.setRequestSuffix('.json');
    RestangularProvider.setDefaultHttpFields({cache: true});
    RestangularProvider.setDefaultRequestParams({ auth_token: '' });
    RestangularProvider.setRequestInterceptor(function(elem, operation) {
      if (operation === "remove") {
         return undefined;
      }
      return elem;
    });
  });

kaityApp.run(function($rootScope) {
    $rootScope.$on('$viewContentLoaded', function () {
      $(document).foundation();
    });
  });